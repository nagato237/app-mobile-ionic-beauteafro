import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private  authService: AuthService,
    private router:Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
        console.log(this.authService.isAuthenticated().then((data)=> {
            if (data == null){
                console.log('VOUS NETES PAS CONNECTE ');

                this.router.navigate(['']);
            }else {
                console.log('VOUS ETES CONNECTE ');
            }

        } ));
    // if (this.authService.isAuthenticated() == null){
    //     console.log('VOUS NETES PAS CONNECTE ');
    //     this.router.navigate(['']);
    // }else {
    //   console.log(this.authService.isAuthenticated());
    //     console.log('VOUS ETES CONNECTE ');
    // }


    });
  }
}
