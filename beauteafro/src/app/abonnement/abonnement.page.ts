import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import  {AlertController} from "@ionic/angular";


@Component({
  selector: 'app-abonnement',
  templateUrl: './abonnement.page.html',
  styleUrls: ['./abonnement.page.scss'],
})
export class AbonnementPage implements OnInit {

  constructor(public http: HttpClient, public  alertController:AlertController) { }

  ngOnInit() {
  }

    save_abonnement(form){
        let intitule = form.value.intitule;
        let prix  = form.value.prix;
        let url = "http://localhost:8000/api/saveaboonement/";
        let data = {intitule: intitule, price:prix };
        let post =  this.http.post(url,data, {headers: new  HttpHeaders({"Content-Type":"application/json"})} ) ;
        post.subscribe((donnee_renvoye)=>{
            this.alerte('Coool', "L'Abonnement " + intitule +" au prix de : "+ prix+" a été crée avec succes", 'Success' );
            console.log(donnee_renvoye);
        } );
    }

   async  alerte(header, message, sub){
      const alert = await this.alertController.create({
            header: header,
            subHeader: sub,
            message: message,
            buttons: ['OK']
        });

      await alert.present();
      let result = await alert.onDidDismiss();
      console.log(result);

    }


}
