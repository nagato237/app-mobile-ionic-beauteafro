import { Component, OnInit } from '@angular/core';
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.scss'],
})
export class RegisterComponentComponent implements OnInit {

  private utilisateur;
  private password;
  constructor(public http : HttpClient, public auth : AuthService) { }

  ngOnInit() {}

    connexion(form){

        let base = "http://192.168.1.33:8000/";
        let  chemin = "api/login_check";
        let url = base + chemin;
        this.password = form.value.password;
        this.utilisateur  = form.value.utilisateur;
        let data = {username:this.utilisateur, password :this.password };
        let post =  this.http.post(url,data, {headers: new  HttpHeaders({"Content-Type":"application/json"})} ) ;
        post.subscribe((data:any)=>{
                this.auth.login(data.token);
                this.auth.setNom(this.utilisateur);
                this.auth.alerte( 'Coool', "Connexion Reussi avec succes", 'Success' );

        }, (error: any)=>{
            this.auth.alerte( 'OoooHoooh', "Verifiez vos identifiants, une erreur est survenue: "+ error.error.message, 'Erreur' );
        } );
    }
}
