import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {

    url_home = "http://192.168.1.33:8000/homevideos";
    videos = [];

  constructor(public http: HttpClient, public auth : AuthService, public navCtrl: NavController) {
      this.http.get(this.url_home, )
          .subscribe((data:any)=> {
              this.videos =  data;

          }) ;

  }

  ngOnInit() {
  }

    gotovideo(item){
        this.navCtrl.navigateForward(['/video', {id: JSON.stringify(item)}]);
    }
}
