import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';


@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {
    myId = null;
    url_home = "http://192.168.1.33:8000/api/video/";
    video = null;
    token = null;
  constructor(private activatedRoute: ActivatedRoute, public http: HttpClient, public auth : AuthService,  private router:Router, private streamingMedia: StreamingMedia, public youtube: YoutubeVideoPlayer) {
      this.myId =activatedRoute.snapshot.params['id'];

      auth.getstringtoken().then(data=>{
          this.token = data;
          const httpOptions = {
              headers: new HttpHeaders({
                  'Authorization': 'Bearer ' + this.token
              })
          };
          this.http.get(this.url_home + this.myId,httpOptions )
              .subscribe((data:any)=> {
                      this.video =  data;
                  this.youtube.openVideo(data.path);
                  },(err) => {
                      if (err.status == 403){
                          this.auth.alerte( 'Sorry', "Desolé vous n'avez pas d'abonnement ", 'Error' );
                          this.router.navigate(['books']);
                      }
                      if(err.status == 401){
                          this.auth.alerte( 'Sorry', "Desolé Votre session est terminée vous veuillez vous connecter de nouveau", 'Error' );
                          this.auth.logout();
                          this.router.navigate(['']);
                      }
                  }
              ) ;
      });

  }

  ngOnInit() {
  }

        lirevideo(video){
            let options: StreamingVideoOptions = {
                successCallback: () => { console.log('Video played') },
                errorCallback: (e) => { console.log('Error streaming') },
                orientation: 'portrait',
                shouldAutoClose: false,
                controls: true
            };

            this.streamingMedia.playVideo(video.path, options);
        }

}
