import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AlertController} from "@ionic/angular";

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.page.html',
  styleUrls: ['./categorie.page.scss'],
})
export class CategoriePage implements OnInit {

  constructor(public http: HttpClient, public  alertController:AlertController) { }

  ngOnInit() {
  }

    save_categorie(form){
        let intitule = form.value.intitule;
        let description  = form.value.description;
        let url = "http://192.168.1.33:8000/api/categorie/";
        let data = {intitule: intitule, description:description };
        let post =  this.http.post(url,data, {headers: new  HttpHeaders({"Content-Type":"application/json"})} ) ;
        post.subscribe((donnee_renvoye)=>{
            this.alerte('Coool', "La categorie " + intitule + "a été crée avec succes", 'Success' );
            console.log(donnee_renvoye);
        } );
    }

    async  alerte(header, message, sub){
        const alert = await this.alertController.create({
            header: header,
            subHeader: sub,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
        let result = await alert.onDidDismiss();
        console.log(result);

    }

}
