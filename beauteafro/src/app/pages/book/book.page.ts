import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {NavParams} from "@ionic/angular";
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import {DocumentViewerOptions} from "@ionic-native/document-viewer";
import { File } from "@ionic-native/file/ngx";
import {
    FileTransfer,
    FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { ModalController } from '@ionic/angular';
import {ModalCommentairePage} from "../modal-commentaire/modal-commentaire.page"


@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {
    myId = null;
    url =  "http://192.168.1.33:8000/";
    url_home = this.url+ "api/livre/chapitre/";
    chapitres = null;
    token = null;
    fileTransfer: FileTransferObject;

  constructor(private activatedRoute: ActivatedRoute, public http: HttpClient, public auth : AuthService,  private router:Router,private document: DocumentViewer, private fileOpener: FileOpener,
              private transfer: FileTransfer, private file: File, private modal:ModalController) {
      this.myId =activatedRoute.snapshot.params['id'];

        auth.getstringtoken().then(data=>{
        this.token = data;
            const httpOptions = {
                headers: new HttpHeaders({
                    'Authorization': 'Bearer ' + this.token
                })
            };
            this.http.get(this.url_home + this.myId,httpOptions )
                .subscribe((data:any)=> {
                        this.chapitres =  data;
                       
                    },(err) => {
                        if (err.status == 403){
                            this.auth.alerte( 'Sorry', "Desolé vous n'avez pas d'abonnement ", 'Error' );
                            this.router.navigate(['books']);
                        }
                        if(err.status == 401){
                            this.auth.alerte( 'Sorry', "Desolé Votre session est terminée,  veuillez vous connecter de nouveau", 'Error' );
                            this.auth.logout();
                            this.router.navigate(['']);
                        }
                    }
                ) ;
      });


  }

  ngOnInit() {

  }

  lirepdf(path){
      const options: DocumentViewerOptions = {
          title: 'My PDF'
      };
        console.log(path);
      this.document.viewDocument(path, 'application/pdf', options)
  }

    download(urli: string) {
        this.fileTransfer = this.transfer.create();
        this.fileTransfer
            .download(this.url+ urli, this.file.dataDirectory + "BeauteAfro" + ".pdf")
            .then(entry => {
                console.log("download complete: " + entry.toURL());
                this.fileOpener
                    .open(entry.toURL(), "application/pdf")
                    .then(() => console.log("File is opened"))
                    .catch(e => console.log("Error opening file", e));
            });
    }

    messagesmodal(id){
        this.modal.create({component:ModalCommentairePage, componentProps:{ 'chapitreid': id}}).then((modalElement) =>{
            modalElement.present();
        })
    }

}
