import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbonementPage } from './abonement.page';

const routes: Routes = [
  {
    path: '',
    component: AbonementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AbonementPageRoutingModule {}
