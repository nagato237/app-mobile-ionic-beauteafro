import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-abonement',
  templateUrl: './abonement.page.html',
  styleUrls: ['./abonement.page.scss'],
})
export class AbonementPage implements OnInit {


  url= "http://192.168.1.33:8000/";
  abonement_url = "abonnements/";
  abonnements = [];

  constructor(public http: HttpClient) { 

    this.http.get(this.url + this.abonement_url, )
        .subscribe((data:any)=> {
          this.abonnements =  data;
        }) ;
  }

  ngOnInit() {
  }

  prendre_abonnement(form){

  }

}
