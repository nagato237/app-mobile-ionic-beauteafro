import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AbonementPageRoutingModule } from './abonement-routing.module';

import { AbonementPage } from './abonement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AbonementPageRoutingModule
  ],
  declarations: [AbonementPage]
})
export class AbonementPageModule {}
