import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AbonementPage } from './abonement.page';

describe('AbonementPage', () => {
  let component: AbonementPage;
  let fixture: ComponentFixture<AbonementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AbonementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
