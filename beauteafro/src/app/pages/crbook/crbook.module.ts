import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrbookPageRoutingModule } from './crbook-routing.module';

import { CrbookPage } from './crbook.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrbookPageRoutingModule
  ],
  declarations: [CrbookPage]
})
export class CrbookPageModule {}
