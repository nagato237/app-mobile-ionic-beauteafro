import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrbookPage } from './crbook.page';

describe('CrbookPage', () => {
  let component: CrbookPage;
  let fixture: ComponentFixture<CrbookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrbookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrbookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
