import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrbookPage } from './crbook.page';

const routes: Routes = [
  {
    path: '',
    component: CrbookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrbookPageRoutingModule {}
