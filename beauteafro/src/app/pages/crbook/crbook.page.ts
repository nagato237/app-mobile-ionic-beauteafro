import { Component, OnInit } from '@angular/core';
import {FileTransfer, FileUploadOptions, FileTransferObject} from "@ionic-native/file-transfer/ngx";
import {FileChooser} from "@ionic-native/file-chooser/ngx";
import {File} from "@ionic-native/file/ngx";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {FilePath} from "@ionic-native/file-path/ngx";
import {AlertController} from "@ionic/angular";
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-crbook',
  templateUrl: './crbook.page.html',
  styleUrls: ['./crbook.page.scss'],
})
export class CrbookPage implements OnInit {
filetransfert : FileTransferObject;
cheminfichier = null;
url = "http://192.168.1.33:8000/";
livre_url = "api/savelivre/";
categories_url = "categories/";
categories = [];
token = null;
  constructor(private  transfert: FileTransfer, private  file: File,  private  filepath: FilePath, private  filechooser: FileChooser, public http: HttpClient, public  alertController:AlertController,  public auth : AuthService) {
    this.auth = auth;
    this.http.get(this.url + this.categories_url, )
    .subscribe((data:any)=> {
      this.categories =  data;
    }) ;
   }

  ngOnInit() {

  }

    save_livre(form){
        let titre = form.value.titre;
        let description  = form.value.description;
        let categorie  = form.value.categorie;

        let data = {titre: titre, description:description, categorie:categorie, miniature:this.cheminfichier };
        console.log(data);

        this.auth.getstringtoken().then(token_res=>{
          this.token = token_res;
              const httpOptions = {
                  headers: new HttpHeaders({
                      'Authorization': 'Bearer ' + this.token,
                      "Content-Type":"application/json"
                  })
              };
              this.http.post(this.url + this.livre_url,data,httpOptions )
                  .subscribe((data:any)=> {
                          
                    console.log(data);
                    this.auth.alerte('Coool', "Le livre " + titre + "a été crée avec succes", 'Success' );   
                      },(err) => {
                          if (err.status == 403){
                              this.auth.alerte( 'Sorry', "Desolé vous n'avez pas d'abonnement ", 'Error' );
      
                          }
                          if(err.status == 401){
                              this.auth.alerte( 'Sorry', "Desolé Votre session est terminée,  veuillez vous connecter de nouveau", 'Error' );
                    
                          }
                      }
                  ) ;
        });

    }


  choisirfichier(){
    this.filechooser.open().then((uri)=>{
      this.filepath.resolveNativePath(uri).then(
          (nativepath)=>{
            this.filetransfert = this.transfert.create();
            let options : FileUploadOptions={
              fileKey: 'imagefile',
                fileName: 'miniature.jpg',
                chunkedMode: false,
                headers:{},
                mimeType:'image/jpeg'
            };
            this.cheminfichier = nativepath;
            console.log(this.cheminfichier);
          //   this.filetransfert.upload(nativepath, "adresse", options).then((data)=>{
          //     alert("ok "+ JSON.stringify(data));
          //
          // }, (err)=>{
          //       alert("error1 "+ JSON.stringify(err));
          //   })
          }, (err)=>{
              alert("error2 "+ JSON.stringify(err));
          })
    }, (err)=>{
        alert("error3 "+ JSON.stringify(err));
    })
  }


}
