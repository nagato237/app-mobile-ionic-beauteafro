import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-factures',
  templateUrl: './factures.page.html',
  styleUrls: ['./factures.page.scss'],
})
export class FacturesPage implements OnInit {

    url_home = "http://192.168.1.33:8000/api/factures";
    factures = [];

    constructor(public http: HttpClient, public auth : AuthService) {
        this.http.get(this.url_home, )
            .subscribe((data:any)=> {
                this.factures =  data;
            }) ;
    }

  ngOnInit() {

  }

}
