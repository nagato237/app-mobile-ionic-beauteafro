import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-books',
  templateUrl: './books.page.html',
  styleUrls: ['./books.page.scss'],
})
export class BooksPage implements OnInit {
    url_home = "http://192.168.1.33:8000/homelivres";
    livres = [];
  constructor(public http: HttpClient, public auth : AuthService, public navCtrl: NavController ) {

      this.http.get(this.url_home, )
          .subscribe((data:any)=> {
              this.livres =  data;

          }) ;
  }

  ngOnInit() {
  }

    gotobook(item){
        this.navCtrl.navigateForward(['/book', {id: JSON.stringify(item)}]);
    }


}
