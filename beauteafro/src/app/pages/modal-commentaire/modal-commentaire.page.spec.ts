import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalCommentairePage } from './modal-commentaire.page';

describe('ModalCommentairePage', () => {
  let component: ModalCommentairePage;
  let fixture: ComponentFixture<ModalCommentairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCommentairePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalCommentairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
