import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";


@Component({
  selector: 'app-modal-commentaire',
  templateUrl: './modal-commentaire.page.html',
  styleUrls: ['./modal-commentaire.page.scss'],
})
export class ModalCommentairePage implements OnInit {

  @Input() chapitreid: string;
  url =  "http://192.168.1.33:8000/";
  url_commentaire = this.url+ "commentaire/";
  url_save_commentaire = this.url+ "savecommentaire/";
  commentaires = [];

  constructor(private modalcontroller:ModalController, public http: HttpClient, public auth : AuthService) { }

  ngOnInit() {

    this.http.get(this.url_commentaire + this.chapitreid )
                .subscribe((data:any)=> {
                        this.commentaires =  data;
                       
                    },(err) => {
                            this.auth.alerte( 'Sorry', "Une erreur est survenue Veuillez contacter l'admin ", 'Error' );

                    }
                ) ;
  }

  closemodal() {
    this.modalcontroller.dismiss();
  }

  save_commentaire(form){
    let chapitre = this.chapitreid;
    let contenu  = form.value.contenu;

    let data = {chapitre: chapitre, contenu:contenu};

    this.http.post(this.url_save_commentaire , data )
    .subscribe((data:any)=> {
             this.commentaires.push(data);
        },(err) => {
                this.auth.alerte( 'Sorry', "Une erreur est survenue Veuillez contacter l'admin ", 'Error' );

        }
    ) ;
  }

}
