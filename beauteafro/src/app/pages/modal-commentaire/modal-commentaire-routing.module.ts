import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalCommentairePage } from './modal-commentaire.page';

const routes: Routes = [
  {
    path: '',
    component: ModalCommentairePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalCommentairePageRoutingModule {}
