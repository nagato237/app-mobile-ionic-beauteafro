import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalCommentairePageRoutingModule } from './modal-commentaire-routing.module';

import { ModalCommentairePage } from './modal-commentaire.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalCommentairePageRoutingModule
  ],
  declarations: [ModalCommentairePage]
})
export class ModalCommentairePageModule {}
