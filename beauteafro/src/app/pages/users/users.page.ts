import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";


@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
url= "http://192.168.1.33:8000/api/users/";
users = null;
data = null;
  constructor(public http: HttpClient) {

      this.http.get(this.url)
          .subscribe((data:any)=> {
             this.users =  data;
              console.log(this.users);

          }) ;
  }

  ngOnInit() {
  }
    supprimer(id){
      let url = "http://localhost:8000/api/delete_user/" + id;

        let post =  this.http.delete(url,{headers: new  HttpHeaders({"Content-Type":"application/json"})} ) ;
        post.subscribe((donnee_renvoye)=>{
            // console.log(donnee_renvoye);
            this.users = this.users.filter(std => std.id !== id);
        } );
    }
}
