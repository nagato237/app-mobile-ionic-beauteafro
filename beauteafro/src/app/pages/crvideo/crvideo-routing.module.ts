import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrvideoPage } from './crvideo.page';

const routes: Routes = [
  {
    path: '',
    component: CrvideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrvideoPageRoutingModule {}
