import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrvideoPage } from './crvideo.page';

describe('CrvideoPage', () => {
  let component: CrvideoPage;
  let fixture: ComponentFixture<CrvideoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrvideoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrvideoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
