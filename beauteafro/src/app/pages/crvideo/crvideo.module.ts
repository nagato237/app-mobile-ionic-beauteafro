import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrvideoPageRoutingModule } from './crvideo-routing.module';

import { CrvideoPage } from './crvideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrvideoPageRoutingModule
  ],
  declarations: [CrvideoPage]
})
export class CrvideoPageModule {}
