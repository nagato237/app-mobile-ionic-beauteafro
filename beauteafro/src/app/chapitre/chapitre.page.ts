import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-chapitre',
  templateUrl: './chapitre.page.html',
  styleUrls: ['./chapitre.page.scss'],
})
export class ChapitrePage implements OnInit {
    livres = [];
    url_home = "http://192.168.1.33:8000/homelivres";

  constructor(public http: HttpClient, public auth : AuthService) {

      this.http.get(this.url_home, )
          .subscribe((data:any)=> {
              this.livres =  data;

          }) ;
  }

  ngOnInit() {
  }

}
