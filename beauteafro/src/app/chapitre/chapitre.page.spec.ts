import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChapitrePage } from './chapitre.page';

describe('ChapitrePage', () => {
  let component: ChapitrePage;
  let fixture: ComponentFixture<ChapitrePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapitrePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChapitrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
