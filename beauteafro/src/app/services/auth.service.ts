import { Injectable } from '@angular/core';
import {AlertController, NavController} from "@ionic/angular";
import { Storage } from '@ionic/storage';
import {BehaviorSubject} from "rxjs/index";


const TOKEN_KEY = null;
const USERNAME = 'Default';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  nom = null;
  statut = new BehaviorSubject(false);

  constructor( public  alertController:AlertController, public storage:Storage, public navCtrl:NavController) {
        storage.create();
  }


    async  alerte(header, message, sub){
        const alert = await this.alertController.create({
            header: header,
            subHeader: sub,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
        let result = await alert.onDidDismiss();
        console.log(result);

    }

    login(token){
      return this.storage.set(TOKEN_KEY, token).then(res =>
      {
          this.statut.next(true);
      });
    }

    async gettoken(){

     const retour =  await this.storage.get(TOKEN_KEY);
        console.log('test ' +retour);
        if (retour == null){
            return null;
        }
        else {
            return retour;
        }
    }

    logout(){
        this.storage.remove(USERNAME);
        return this.storage.remove(TOKEN_KEY).then(res =>
        {
            this.statut.next(false);
        });
    }

    async isAuthenticated(){
        const retour = await this.gettoken();
    return retour;
    }



    setNom(nom){
        this.storage.set(USERNAME, nom);
    }

    async getNom(){
        const retour = await  this.storage.get(USERNAME);
        return retour;

    }

    getstringtoken (){
        let allo = this.storage.get(TOKEN_KEY).then((data)=>{
            return data;
        });
        return allo;
    }
}
