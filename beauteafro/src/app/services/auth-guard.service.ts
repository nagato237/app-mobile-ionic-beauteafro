import { Injectable } from '@angular/core';
import {CanActivate} from "@angular/router";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor( private  authService:AuthService) { }

    canActivate(): any{
    console.log(this.authService.isAuthenticated());
      let allo = this.authService.isAuthenticated().then((data)=>{
         if (data == null){
             this.authService.alerte( 'Desolé', "Pensez a prendre un abonnement", '' );

             return false;
         }else {
             return true;
         }
       });
    return allo;
  }
}
