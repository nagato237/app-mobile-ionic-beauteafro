import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";

const routes: Routes = [

    {
        path: 'video',
        loadChildren: () => import('../pages/video/video.module').then( m => m.VideoPageModule)
    },

    {
        path: 'book',
        loadChildren: () => import('../pages/book/book.module').then( m => m.BookPageModule)
    },

];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class MembersRootingModule { }
