import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'connexion',
    loadChildren: () => import('./pages/connexion/connexion.module').then( m => m.ConnexionPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./pages/videos/videos.module').then( m => m.VideosPageModule)
  },

  {
    path: 'books',
    loadChildren: () => import('./pages/books/books.module').then( m => m.BooksPageModule)
  },

  {
    path: 'crbook',
    loadChildren: () => import('./pages/crbook/crbook.module').then( m => m.CrbookPageModule)
  },
  {
    path: 'crvideo',
    loadChildren: () => import('./pages/crvideo/crvideo.module').then( m => m.CrvideoPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminPageModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./pages/users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'abonnement',
    loadChildren: () => import('./abonnement/abonnement.module').then( m => m.AbonnementPageModule)
  },
  {
    path: 'categorie',
    loadChildren: () => import('./pages/categorie/categorie.module').then( m => m.CategoriePageModule)
  },
  {
    path: 'abonement',
    loadChildren: () => import('./pages/abonement/abonement.module').then( m => m.AbonementPageModule)
  },
  {
    path: 'chapitre',
    loadChildren: () => import('./chapitre/chapitre.module').then( m => m.ChapitrePageModule)
  },
    {
        path: 'video',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./pages/video/video.module').then( m => m.VideoPageModule)
    },

    {
        path: 'book',
        canActivate: [AuthGuardService],
        loadChildren: () => import('./pages/book/book.module').then( m => m.BookPageModule)
    },
  {
    path: 'factures',
    loadChildren: () => import('./pages/factures/factures.module').then( m => m.FacturesPageModule)
  },
  {
    path: 'modal-commentaire',
    loadChildren: () => import('./pages/modal-commentaire/modal-commentaire.module').then( m => m.ModalCommentairePageModule)
  }
    //,

    // {
    //   path: 'membres',
    //     canActivate: [AuthGuardService],
    //     loadChildren: () => import('./members/members-rooting.module').then( m => m.MembersRootingModule)
    // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
