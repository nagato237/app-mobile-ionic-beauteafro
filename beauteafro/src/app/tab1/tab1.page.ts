import { Component } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {any} from "codelyzer/util/function";
import {AuthService} from "../services/auth.service";


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  url_home = "http://192.168.1.33:8000/home";

  videos = [];
  livres = [];
    data = null;

  constructor(public http: HttpClient, public auth : AuthService ) {

    this.http.get(this.url_home, )
        .subscribe((data:any)=> {

          this.livres =  data.livres;
          this.videos =  data.videos;
        }) ;


  }


}
