import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-parametre-component',
  templateUrl: './parametre-component.component.html',
  styleUrls: ['./parametre-component.component.scss'],
})
export class ParametreComponentComponent implements OnInit {

  nom:any = "nguegang";
  prenom= "franklin";
  constructor( public auth : AuthService, private router:Router) { }

  ngOnInit() {
    // this.nom = this.auth.getNom();
  }

  deconnexion(){
      this.auth.logout();
      this.auth.alerte( 'Cool', "Vous etes a present deconnecté ", 'Success' );
      this.router.navigate(['']);
  }



}
