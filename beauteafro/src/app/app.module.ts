import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from "@angular/common/http";
import {FileTransfer} from "@ionic-native/file-transfer/ngx";
import {FileChooser} from "@ionic-native/file-chooser/ngx";
import {File} from "@ionic-native/file/ngx";
import {FilePath} from "@ionic-native/file-path/ngx";
import {AuthService} from "./services/auth.service";
import { Storage } from '@ionic/storage';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from "@ionic-native/file-opener/ngx";
import {ModalCommentairePageModule} from "./pages/modal-commentaire/modal-commentaire.module"




@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
      BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, ModalCommentairePageModule],
  providers: [
    StatusBar,
    SplashScreen,
      FileTransfer,
      FileChooser,
      File,
      FilePath,
      AuthService,
      Storage,
      StreamingMedia,
      YoutubeVideoPlayer,
      DocumentViewer,
      FileTransfer,
      FileOpener,
      File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
