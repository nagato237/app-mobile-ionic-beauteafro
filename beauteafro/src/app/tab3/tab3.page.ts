  import { Component } from '@angular/core';
  import {LoadingController} from "@ionic/angular";
  import {AuthService} from "../services/auth.service";


  @Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
  })
  export class Tab3Page {
    statut: any = false;
    constructor(public loadingctrl: LoadingController, public auth:AuthService) {

    }

      ngOnInit(){
        this.statut = this.auth.isAuthenticated().then((data)=>{
            if (data == null){
                return false;
            }else {
                return true;
            }
        });

    }

  }
